<?php namespace Cyg\UtileriasWeb;

class Base64UrlEncoder
{
    public static function encode($data)
    {
        $base64 = base64_encode($data);
        $base64url = str_replace(['+', '/', '='], ['-', '_', ''], $base64);
        return $base64url;
    }

    public static function decode($data)
    {
        $base64 = str_replace(['-', '_'], ['+', '/'], $data);
        $padding = strlen($base64) % 4;
        if ($padding) {
            $base64 .= str_repeat('=', 4 - $padding);
        }
        return base64_decode($base64);
    }
}

?>